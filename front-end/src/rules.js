export default {
    required: val => !!val || "Campo obrigatório.",
  
    requiredList: val => val.length > 0 || "Campo obrigatório.",
  
    equalPassword: getPassword => val =>
      val == getPassword() || "As senhas não estão iguais.",
  
    passwordMin: val =>
      (val != undefined && val.length >= 8) ||
      "A senha deve conter pelo menos 8 dígitos.",
  
    cep: val =>
      (val != undefined && val.length == 9) ||
      "O CEP deve conter 9 números.",
  
    cpf: val => !val || ((val || '').length == 14 || "Estão faltando números do CPF."),
  
    cnpj: val => !val || ((val || '').length == 18 || "Estão faltando números do CNPJ."),
  
    cnpj_cpf: val => !val || ([14,18].includes((val || '').length) || "Estão faltando números."),
  
    phone: val => !val || ([14,15].includes((val || '').length) || "Estão faltando números do telefone."),
    
    email: val =>
      (!val || /^([a-zA-Z][a-zA-Z0-9_.-]*@([a-zA-Z][a-zA-Z0-9_-]*)(\.[a-zA-Z][a-zA-Z0-9_-]*[a-zA-Z])+)?$/g.test(val))
      || "Insira um e-mail válido.",
  
    date: val =>
      !val ||
      (/^([0-9]{2}\/){2}[0-9]{4}$/g.test(val) &&
        (parseInt(val.split("/")[0]) <= 31 &&
          parseInt(val.split("/")[1]) <= 12 &&
          parseInt(val.split("/")[2]) > 999)) ||
      "Insira uma data válida.",
  }
  