import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import rules from '@/rules'

Vue.config.productionTip = false
Vue.prototype.$rules = rules;

Vue.prototype.$axios = require('axios').create({
  baseURL: process.env.NODE_ENV === "production" ? "/api" : "http://localhost:8081/",
  withCredentials: false,
});



new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')











