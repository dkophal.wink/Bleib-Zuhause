---========CRIAR TABELAS=========---
CREATE TABLE ENDERECO (
CEP VARCHAR NOT NULL PRIMARY KEY,
NOME_CID VARCHAR(255) NOT NULL,
UF VARCHAR NOT NULL
);

CREATE TABLE EMPRESA (
CNPJ VARCHAR NOT NULL PRIMARY KEY,
RAZAO_SOCIAL VARCHAR(255) NOT NULL
);

CREATE TABLE USUARIO (
USU_ID SERIAL NOT NULL PRIMARY KEY, 
EMAIL VARCHAR(255) NOT NULL UNIQUE,
SENHA VARCHAR(255) NOT NULL,
NOME VARCHAR(255) NOT NULL,
SEXO CHAR NOT NULL,
DAT_NASC DATE NOT NULL,
CARGO VARCHAR(255) NOT NULL,
IS_ADMIN BOOLEAN DEFAULT FALSE,
POSITIVADO BOOLEAN DEFAULT FALSE,
SUSPEITA BOOLEAN DEFAULT FALSE,
CNPJ VARCHAR NOT NULL,
CONSTRAINT EMP_CNPJEMP_FK FOREIGN KEY(CNPJ) REFERENCES EMPRESA(CNPJ),
CEP VARCHAR NOT NULL,
CONSTRAINT CEP_ENDERECO_FK FOREIGN KEY(CEP) REFERENCES ENDERECO(CEP)
);

CREATE TABLE PRONTUARIO (
PRONT_ID SERIAL NOT NULL PRIMARY KEY,
DATA_SINT DATE NOT NULL DEFAULT CURRENT_DATE,
USU_ID INT4 NOT NULL,
CONSTRAINT PRONT_USU_FK FOREIGN KEY(USU_ID) REFERENCES USUARIO(USU_ID)
);

CREATE TABLE SINTOMA (
SINT_ID SERIAL NOT NULL PRIMARY KEY,
NOME_SINT VARCHAR(255) NOT NULL
);

 CREATE TABLE PRONTUARIO_SINTOMA(
 	PROSINCOD SERIAL PRIMARY KEY,
 	SINT_ID INT4 CONSTRAINT SINTOMA_FK REFERENCES SINTOMA(SINT_ID),
 	PRONT_ID INT4 CONSTRAINT PROTUARIO_FK REFERENCES PRONTUARIO(PRONT_ID)
 );


--=====================================REGRAS========================================----

--==O MESMO USU�RIO N�O CRIAR UM SINTOMA QUE TEM 0 MESMO NOME E A MESMA DATA (ID_PROTU�RIO)==--
CREATE RULE NOVO_SINTOMA AS ON INSERT TO SINTOMA
  WHERE EXISTS 
  (SELECT 1 FROM SINTOMA S 
	WHERE (PRONT_ID, NOME_SINT) = (NEW.PRONT_ID, NEW.NOME_SINT))
  DO INSTEAD NOTHING;
 
 
 --==O PRIMEIRO USU�RIO QUE SE CADASTRAR NA EMPRESA SEMPRE VAI SER ADMIN==--
 

----==================================RELAT�RIOS=========================================----


--1) RELACIONE O C�DIGO E NOME DE PACIENTES COM IDADES ENTRE 40 E 50,
--QUE APRESENTARAM FALTA DE AR. RELACIONE A CONSULTA EM ORDEM ASCENDENTE DE NOME;

create view vw_select1 as
select u.usu_id , u.nome, extract(year from age(current_date,u.dat_nasc)) :: int as idade from usuario u
inner join prontuario p on p.usu_id = u.usu_id 
inner join prontuario_sintoma ps on ps.pront_id = p.pront_id 
inner join sintoma s on s.sint_id = ps.sint_id 
where extract(year from age(current_date,u.dat_nasc)) :: int >= 40 and 
extract(year from age(current_date,u.dat_nasc)) :: int <= 50 and s.nome_sint = 'falta de ar'
order by u.nome asc;

--2) Relacione o nome do paciente, cep da cidade de resid�ncia de pacientes
--do sexo masculino, residentes nos munic�pios de Maravilha, Descanso,
--Pinhalzinho, Chapec� e Itapiranga que apresentaram sintomas e foram
--positivados com covid. Relacione o relat�rio pelo cep da cidade
--descendente e o nome do paciente ascendente;
create view vw_select2 as
SELECT U.NOME, E.CEP, E.NOME_CID FROM USUARIO U
LEFT JOIN ENDERECO E ON U.CEP = E.CEP 
WHERE (E.NOME_CID = 'Maravilha' 
OR E.NOME_CID = 'Pinhalzinho' 
OR E.NOME_CID = 'Descanso' 
OR E.NOME_CID = 'Chapec�'
OR E.NOME_CID = 'Itapiranga') 
AND U.POSITIVADO AND U.SEXO = 'M' AND SUSPEITA
ORDER BY E.CEP DESC, U.NOME ASC;


--3) Relacione o cep da cidade, nome da cidade e quantidade de casos
--suspeitos de covid para as cidades com 5 � 10 casos. Ordene o relat�rio
--da cidade com mais casos suspeitos para a cidade com menos casos suspeitos;
create view vw_select3 as
SELECT U.CEP, E.NOME_CID, COUNT(SUSPEITA OR NULL) AS SUSPEITAS FROM USUARIO U 
LEFT JOIN ENDERECO E ON U.CEP = E.CEP 
GROUP BY U.CEP, E.NOME_CID 
HAVING COUNT(U.POSITIVADO OR NULL) >= 5 AND COUNT(U.POSITIVADO OR NULL) <= 10
ORDER BY COUNT(SUSPEITA OR NULL) DESC;


--4) Relacione a idade e quantidade de casos positivos de covid por idade.
--Somente idades com mais de 20 casos. Ordene o relat�rio pela idade
--com menos casos para a idade com mais casos.
create view vw_select4 as
SELECT EXTRACT(YEAR FROM AGE(CURRENT_DATE,U.DAT_NASC)) :: INT AS IDADE, COUNT(POSITIVADO OR NULL) AS POSITIVADOS
FROM USUARIO U
GROUP BY EXTRACT(YEAR FROM AGE(CURRENT_DATE,U.DAT_NASC)) :: INT
HAVING COUNT(U.POSITIVADO OR NULL) >= 20
ORDER BY COUNT(U.POSITIVADO OR NULL) ASC;



--DROP TABLE ENDERECO CASCADE;
DROP TABLE EMPRESA CASCADE;
DROP TABLE USUARIO CASCADE;
DROP TABLE PRONTUARIO CASCADE;
DROP TABLE SINTOMA CASCADE;

SELECT CNPJ FROM EMPRESA;
SELECT USU_ID FROM EMPRESA E;
select * from endereco e;
INNER JOIN USUARIO U ON U.CNPJ = E.CNPJ;
DROP RULE NOVO_SINTOMA ON SINTOMA;
DROP RULE PRIMEIRO_USUARIO ON USUARIO;


select * from sintoma;

nextval('prontuario_pront_id_seql')
